R-vterm.el:
	@wget https://raw.githubusercontent.com/shg/julia-vterm.el/master/julia-vterm.el 
	mv julia-vterm.el R-vterm.el
	sed -i 's/extends julia-mode/extends ess-r-mode/g' R-vterm.el
	sed -i 's/julia/R/g' R-vterm.el
	sed -i 's/Julia/R/g' R-vterm.el

clean:
	rm *.el
